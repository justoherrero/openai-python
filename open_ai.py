import os
import openai
import logging
import argparse


"""

args.message: Contains the message to be sent to the OPEN AI API

"""


def main(args: argparse.Namespace = None) -> None:
    """
    Main function for our code.

    Parameters
    ----------
    args: argparse.Namespace
        Contains the arguments content (if exists)

    Returns
    -------
    None
    """

    openai.api_key = os.environ['OPENAI_API_KEY']
    #engines = openai.Engine.list()
    # logging.info(engines.data)

    model_engine = "text-davinci-003"
    prompt = args.message

    # Generate a response
    completion = openai.Completion.create(
        engine=model_engine,
        prompt=prompt,
        max_tokens=2048,
        n=1,
        stop=None,
        temperature=0.5,
    )

    response = completion.choices[0].text
    logging.info(response)


def parse_arguments() -> argparse.Namespace:
    """
    Read arguments from a command line.

    Parameters
    ----------
    Not arguments required.

    Returns
    -------
    :argparse.Namespace
        Contains the structure with the arguments content.

    """
    parser = argparse.ArgumentParser(
        description='Arguments get parsed via --commands')
    parser.add_argument(
        '-message',
        metavar='message_to_open_ai_api',
        type=str,
        required=True,
        help='This must contain the message to be sent to the OPEN AI API')
    return parser.parse_args()


if __name__ == "__main__":
    logging.basicConfig(level='INFO')
    logging.info('Initializing OPEN AI Script')
    args = parse_arguments()
    main(args)
    logging.info('OPEN AI Script end')
