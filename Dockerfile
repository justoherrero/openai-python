FROM python:3.10

ARG OPENAI_API_KEY
ENV OPENAI_API_KEY=$OPENAI_API_KEY

ADD open_ai.py /
ADD requirements.txt /
RUN pip install -r requirements.txt

ENTRYPOINT [ "python", "./open_ai.py" ]
