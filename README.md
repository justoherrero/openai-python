# openai-python

## Introduction

Contains a basic approach to use OPEN AI API using Python.

The script receives the message by parameter and show the API response by console.

## Parameters

- **-mesage**: Contains the message to the OPEN AI API.

## Usage

### Docker

- Build the image
  `docker build --build-arg OPENAI_API_KEY -t openai-python .`

- Run the image
  `docker run openai-python -message MESSAGE`

### Python

To use this script you can use the following command:

`python open_ai.py -message MESSAGE`

**NOTE**: Remember that you need to install the **openai** Python library and set up the API KEY as environment variable.

## Contact

If you have any question, you can contact me directly to `jherreroa23@outlook.es`
